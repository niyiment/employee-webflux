### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### Spring WebFlux sample crud
Spring WebFlux framework is part of Spring 5 and provides reactive programming support for web applications. 
This repo is a small reactive REST application.