package com.niyiment.employee.repository;

import com.niyiment.employee.model.Employee;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface EmployeeRepository extends ReactiveMongoRepository<Employee, String> {
    @Query("{ 'firstName': ?0 }")
    Flux<Employee> findByFirstName(final String firstName);

}
