package com.niyiment.employee.service;

import com.niyiment.employee.model.Employee;
import org.springframework.context.ApplicationEvent;

public class EmployeeCreatedEvent extends ApplicationEvent {
    public EmployeeCreatedEvent(Employee employee1) {
        super(employee1);
    }
}
