package com.niyiment.employee.service;

import com.niyiment.employee.model.Employee;
import com.niyiment.employee.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class EmployeeService implements IEmployeeService {

    private final EmployeeRepository employeeRepository;

    private final ApplicationEventPublisher publisher;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, ApplicationEventPublisher publisher) {
        this.employeeRepository = employeeRepository;
        this.publisher = publisher;
    }

    /**
     *
     * @param employee
     * @return employee
     */
    @Override
    public Mono<Employee> create(Employee employee) {
       return employeeRepository.save(employee).doOnSuccess(customer1 -> this.publisher.publishEvent(new EmployeeCreatedEvent(customer1)));
    }

    /**
     *
     * @param id
     * @return employee
     */
    @Override
    public Mono<Employee> findById(String id) {
        return employeeRepository.findById(id);
    }

    /**
     *
     * @param firstName
     * @return 0 or more employees with the same firstname
     */
    @Override
    public Flux<Employee> findByFirstName(String firstName) {
        return employeeRepository.findByFirstName(firstName);
    }

    /**
     *
     * @return list of employees
     */
    @Override
    public Flux<Employee> findAll() {
        return employeeRepository.findAll();
    }

    /**
     *
     * @param employee
     * @return updated employee record
     */
    @Override
    public Mono<Employee> update(Employee employee) {
        return employeeRepository.save(employee);
    }

    /**
     *
     * @param id
     * @return
     */
    @Override
    public Mono<Void> delete(String id) {
        return employeeRepository.deleteById(id);
    }
}
