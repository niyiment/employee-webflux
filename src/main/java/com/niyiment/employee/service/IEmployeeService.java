package com.niyiment.employee.service;

import com.niyiment.employee.model.Employee;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IEmployeeService {
    Mono<Employee> create(Employee employee);

    Mono<Employee> findById(String id);

    Flux<Employee> findByFirstName(String firstName);

    Flux<Employee> findAll();

    Mono<Employee> update(Employee employee);

    Mono<Void> delete(String id);
}
