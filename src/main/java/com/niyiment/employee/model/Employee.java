package com.niyiment.employee.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Employee entity
 */

@Document
@Data
@AllArgsConstructor
public class Employee {
    @Id
    String id;
    String firstName;
    String lastName;
    String email;
    long salary;
}
