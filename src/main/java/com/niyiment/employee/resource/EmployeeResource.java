package com.niyiment.employee.resource;

import com.niyiment.employee.model.Employee;
import com.niyiment.employee.service.EmployeeService;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;

@RestController
@RequestMapping("/employees")
public class EmployeeResource {
    @Autowired
    private final EmployeeService service;

    private static final Logger log = LoggerFactory.getLogger(EmployeeResource.class);

    public EmployeeResource(EmployeeService service) {
        this.service = service;
    }

    /**
     * @code PostMapping
     * @param employee
     * @return employee
     */
    @PostMapping(value = { "/save"})
    @ResponseStatus(HttpStatus.CREATED)
    public Publisher<ResponseEntity<Employee>> create(@RequestBody Employee employee) {
        return service.create(employee)
            .map(c -> ResponseEntity.created(URI.create("/customers/" + c.getId()))
                .contentType(MediaType.APPLICATION_JSON)
                .build());

    }

    /**
     * @code GetMapping
     * @param id
     * @return zero or one employee as json
     */
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Mono<Employee>> findById(@PathVariable("id") String id) {
        Mono<Employee> employee = service.findById(id);
        HttpStatus status = employee != null ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<Mono<Employee>>(employee, status);
    }

    /**
     * @code GetMapping
     * @param name
     * @return zero or more services with the same firstname as json
     */
    @GetMapping(value = "/name/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<Employee> findByFirstName(@PathVariable("name") String name) {
        return service.findByFirstName(name);
    }

    /**
     * @code GetMapping
     * @return list of employees as json
     */
    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public Flux<Employee> findAll() {
        Flux<Employee> employees = service.findAll();
        log.info("Employee flux::::::: ", employees);
        return employees;
    }

    /**
     * @PutMapping
     * @param employee
     * @return employee as json
     */
    @PutMapping(value = "/update", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public Publisher<ResponseEntity<Employee>> update(@RequestBody Employee employee)
    {
        return Mono.just(employee).flatMap(c -> this.service.update(employee))
            .map(c -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).build());
    }

    /**
     * @code DeleteMapping
     * @param id
     */
    @DeleteMapping(value = "/delete/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") String id) {
        service.delete(id).subscribe();
    }
}
